<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\User;
use Validator;


class UserController extends Controller
{
      /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (\Gate::allows('isAdmin') || \Gate::allows('isAuthor')) {
            return User::latest()->paginate(2);
        }
       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name'   => 'required|string|max:225',
            'email'  =>'required',
            'password' =>'sometimes|required|string|min:6',
        ]);
        $this->authorize('isAdmin');
        $user=new User;
        $user->name=$request->name;
        $user->email=$request->email;
        $user->password=Hash::make($request->password);
        $user->bio=$request->bio;
        $user->photo=$request->photo;
        $user->type=$request->type;
        $user->save();
        return "0k";
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user=User::findOrFail($id);
        $this->validate($request,[
            'name'   => 'required|string|max:225',
            'email'  =>'required |email',
            'password' =>'sometimes|string|min:6',
        ]);
        $this->authorize('isAdmin');
        $user->update($request->all());
        return ['message'=>'user update'];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->authorize('isAdmin');
        $users=User::find($id);

        $users->delete();

        return ['message'=> 'Delete User'];
    }
    public function userprofile(){
        return auth('api')->user();
    }
    public function updateProfile(Request $request){
        $user= auth('api')->user();
        $password=$user->password;
        $this->validate($request,[
            'name'   => 'required|string|max:225',
            'email'  =>'required',
            'password' =>'sometimes|required|string|min:6',
        ]);
        $currentProfile=$user->photo;
        if($request->photo != $currentProfile){
            $name = time().'.' . explode('/', explode(':', substr($request->photo, 0, strpos($request->photo, ';')))[1])[1];
            \Image::make($request->photo)->save(public_path('images/').$name);
            $request->merge(['photo' => $name]);
            $userPhoto=public_path('images/').$currentProfile;
            if(file_exists($userPhoto)){
                @unlink($userPhoto);
            }
        }
        if(!empty($request->password)){
            $user->password=Hash::make($request->password);
        }
        else{
            $user->password=$password;
        }
        $user->name=$request->name;
        $user->email=$request->email;
        $user->bio=$request->bio;
        $user->photo=$request->photo;
        $user->update();
        return "ok";
    }
    public function search(){
        if ($search = \Request::get('q')) {
            $users = User::where(function($query) use ($search){
                $query->where('name','LIKE',"%$search%")
                        ->orWhere('email','LIKE',"%$search%");
            })->paginate(20);
        }else{
            $users = User::latest()->paginate(5);
        }
        return $users;
    }
}
