<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Books\BookCategory;

class BookCategotyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return BookCategory::latest()->paginate(10);
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request,[
            'category_name'   => 'required |unique:book_categories',
            'description' =>'sometimes',
        ]);
        //$this->authorize('isAdmin');
        $user=new BookCategory;
        $user->category_name=$request->category_name;
        $user->description=$request->description;
        $user->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $categories=BookCategory::findOrFail($id);
        $this->validate($request,[
            'category_name'   => 'required | unique:book_categories,category_name,'. $id,
            'description' =>'sometimes',
        ]);
       // $this->authorize('isAdmin');
        $categories->update($request->all());
        return ['message'=>'Category update'];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        //$this->authorize('isAdmin');
        $category=BookCategory::findOrFail($id);

        $category->delete();

        return ['message'=> 'Delete Category'];
    }
}
