<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Books\BookSubCategory;

class BookSubCategotyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         //if (\Gate::allows('isAdmin') || \Gate::allows('isAuthor')) {
            return BookSubCategory::with('bookCategory')->latest()->paginate(10);
        //}

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //return \response()->json($request->all(), 404);
        //
        $this->validate($request,[
            'name'   => 'required | unique:book_sub_categories',
            'category_id' => 'required',
        ]);
       // $this->authorize('isAdmin');
        $bookSubCategory=new BookSubCategory();
        $bookSubCategory->name=$request->name;
        $bookSubCategory->description =$request->description;
        $bookSubCategory->category_id=$request->category_id;
        $bookSubCategory->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      // return \response()->json($request->all(), 404);

        //
        $bookSubCategory=BookSubCategory::findOrFail($id);
        $this->validate($request,[
            'name'   => 'required | unique:book_sub_categories,name,'.$id,
            'category_id' => 'required',
        ]);
        //$this->authorize('isAdmin');
        $bookSubCategory->update($request->all());
        return ['message'=>'Book Sub Category update'];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        //$this->authorize('isAdmin');
        $bookSubCategory=BookSubCategory::find($id);

        $bookSubCategory->delete();

        return ['message'=> 'Delete Sub Category'];
    }
}
