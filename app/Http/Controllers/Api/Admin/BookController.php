<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Models\Books\Book;
use Illuminate\Http\Request;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return Book::latest()->paginate(10);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       // return \response()->json($request->all(), 404);
        //
        $this->validate($request,[
            'name'   => 'required | unique:books',
            'author_name' =>'required',
            'publication_name' => 'required',
            'publication_name' => 'required',
            'published_date' => 'required',
            'price' => 'required',
            'currency' => 'required',
            'number_of_pices' => 'required',
            'category_id' => 'required',
            'sub_category_id' => 'required',
        ]);
       // $this->authorize('isAdmin');
        $books=new Book;
        $books->insert($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->validate($request,[
            'name'   => 'required | unique:books,name,'. $id,
            'author_name' =>'required',
            'publication_name' => 'required',
            'publication_name' => 'required',
            'published_date' => 'required',
            'price' => 'required',
            'currency' => 'required',
            'number_of_pices' => 'required',
            'category_id' => 'required',
            'sub_category_id' => 'required',
        ]);
       // $this->authorize('isAdmin');
        $books=Book::findOrFail($id);
        $books->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
       // $this->authorize('isAdmin');
        $book=Book::findOrFail($id);

        $book->delete();

        return ['message'=> 'Delete book'];
    }
}
