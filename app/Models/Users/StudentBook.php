<?php

namespace App\Models\Users;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Models\Books\Book;

class StudentBook extends Model
{
    protected $fillable=['user_id','book_id','issue_date','return_date','status'];
    //
    public function userInformation(){
        return $this->belongsTo(User::class,'user_id');
    }
    public function book(){
        return $this->belongsTo(Book::class, 'book_id');
    }
}
