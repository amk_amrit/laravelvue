<?php

namespace App\Models\Users;

use Illuminate\Database\Eloquent\Model;
use phpDocumentor\Reflection\Types\This;

class StudentFine extends Model
{
    protected $fillable=['user_id','book_id','amout'];
    //
    public function userInformation(){
        return $this->belongsTo(User::class,'user_id');
    }
    public function book(){
        return $this->belongsTo(Book::class, 'book_id');
    }
}
