<?php

namespace App\Models\Books;

use Illuminate\Database\Eloquent\Model;

class BookCategory extends Model
{
    protected $fillable =['category_name','description', 'status'];
    //
    public function subCategory(){
        return $this->hasMany(BookSubCategory::class, 'category_id');
    }
    public function book(){
        return $this->hasMany(Book::class, 'category_id');
    }
}
