<?php

namespace App\Models\Books;

use Illuminate\Database\Eloquent\Model;

class BookSubCategory extends Model
{
    protected $fillable=['category_id','name','description','status'];
    //
    public function book(){
        return $this->hasMany(Book::class, 'sub_category_id')->withDefault();
    }
    public function bookCategory(){
        return $this->belongsTo(BookCategory::class,'category_id')->withDefault();
    }
}
