<?php

namespace App\Models\Books;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    protected $fillable=['name','author_name','publication_name', 'published_date','price', 'currency','number_of_pices','description','status','category_id','sub_category_id'];
    //
    public function bookCategory(){
        return $this->belongsTo(BookCategory::class, 'category_id')->withDefault();
    }
    public function bookSubCategory(){
        return $this->belongsTo(BookSubCategory::class, 'sub_category_id')->withDefault();
    }
}
