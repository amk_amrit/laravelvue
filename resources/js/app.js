/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
import moment from 'moment';

import { Form, HasError, AlertError } from 'vform';
window.Form = Form;
Vue.component(HasError.name, HasError);
Vue.component(AlertError.name, AlertError);
//Pagination Function Start 
Vue.component('pagination', require('laravel-vue-pagination'));
//Pagination Function End


import Gate from "./Gate";
Vue.prototype.$gate = new Gate(window.user);



//Prgoress Bar Package Loading
import VueProgressBar from 'vue-progressbar'

const options = {
  color: '#2ae5ea',
  failedColor: '#ea2a3e',
  thickness: '10px',
  transition: {
    speed: '60s',
    opacity: '0.6s',
    termination: 300
  },
  autoRevert: true,
  location: 'top',
  inverse: false
}
Vue.use(VueProgressBar, options)

//Sweet Alert 2 Package Loading Import
import Swal from 'sweetalert2'
window.Swal= Swal;

const Toast = Swal.mixin({
  toast: true,
  position: 'top-end',
  showConfirmButton: false,
  timer: 3000,
  timerProgressBar: true,
  onOpen: (toast) => {
    toast.addEventListener('mouseenter', Swal.stopTimer)
    toast.addEventListener('mouseleave', Swal.resumeTimer)
  }
})
window.Toast = Toast;
//Custome Event Or Refresh Data If new data is enter 
let Fire = new Vue()

window.Fire= Fire;
// This function used for VueRoute start 
import VueRouter from 'vue-router';

Vue.use(VueRouter)

let routes = [
    { path: '/dashboard', component: require('./components/Dastboard.vue').default },
    { path: '/profile', component: require('./components/Profile.vue').default },
    { path: '/users', component: require('./components/Users.vue').default },
    { path: '/categories', component: require('./components/admin/BookCategory.vue').default },
    { path: '/subcategories', component: require('./components/admin/BookSubCategory.vue').default },
    { path: '/books', component: require('./components/admin/Book.vue').default },
    { path: '/studentbooks', component: require('./components/admin/StudentBook.vue').default },
    { path: '/bookreturns', component: require('./components/admin/BookReturn.vue').default },
    { path: '*', component: require('./components/notFound.vue').default }
  ]

  const router = new VueRouter({
    mode: 'history',
    routes // short for `routes: routes`
  })
  // This function used for VueRoute end 

  Vue.filter('upText',function(text){
    return text.charAt(0).toUpperCase() + text.slice(1);
  });
Vue.filter('myDate', function(created){
  return moment(created).format('MMMM Do YYYY'); // December 28th 2019, 12:49:00 pm
});
/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('example-component', require('./components/ExampleComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
//Password Config Function 

Vue.component(
  'passport-clients',
  require('./components/passport/Clients.vue').default
);

Vue.component(
  'passport-authorized-clients',
  require('./components/passport/AuthorizedClients.vue').default
);

Vue.component(
  'passport-personal-access-tokens',
  require('./components/passport/PersonalAccessTokens.vue').default
);
Vue.component(
  'not-found',
  require('./components/notFound.vue').default
);

const app = new Vue({
    el: '#app',
    router,
    data:{
        search:''
    },
    methods:{
      searchit(){
        
        Fire.$emit('searching');
      }
    }
});
