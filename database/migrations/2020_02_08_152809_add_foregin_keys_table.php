<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeginKeysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('book_categories')){
            Schema::table('book_sub_categories', function (Blueprint $table) {
                $table->foreign('category_id')->references('id')->on('book_categories')->onDelete('no action');
            });
            Schema::table('books', function (Blueprint $table) {
                $table->foreign('category_id')->references('id')->on('book_categories')->onDelete('no action');
            });
        }
        if(Schema::hasTable('book_sub_categories')){
            Schema::table('books', function(Blueprint $table){
                $table->foreign('sub_category_id')->references('id')->on('book_sub_categories')->onDelete('no action');
            });
        }
        if(Schema::hasTable('books')){
            Schema::table('student_books', function(Blueprint $table){
                $table->foreign('book_id')->references('id')->on('books')->onDelete('no action');
            });
            Schema::table('student_fines', function(Blueprint $table){
                $table->foreign('book_id')->references('id')->on('books')->onDelete('no action');
            });
        }
        if(Schema::hasTable('users')){
            Schema::table('student_books', function(Blueprint $table){
                $table->foreign('user_id')->references('id')->on('users')->onDelete('no action');
            });
            Schema::table('student_fines', function(Blueprint $table){
                $table->foreign('user_id')->references('id')->on('users')->onDelete('no action');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('foregin_keys', function (Blueprint $table) {
            //
        });
    }
}
