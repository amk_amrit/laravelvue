<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::apiresource('user','Api\UserController');
Route::get('userprofile','Api\UserController@userprofile');
Route::put('profile','Api\UserController@updateProfile');
Route::get('findUser','Api\UserController@search');
//Book Category 
Route::apiresource('books','Api\Admin\BookController');
Route::apiresource('studentbooks','Api\Admin\StudentBookController');
Route::apiresource('bookreturns','Api\Admin\BookReturnController');
Route::group(['prefix' => 'book', 'as' => 'book.'], function() {
    Route::apiresource('categories','Api\Admin\BookCategotyController');
    Route::apiresource('subcategories','Api\Admin\BookSubCategotyController');

});

